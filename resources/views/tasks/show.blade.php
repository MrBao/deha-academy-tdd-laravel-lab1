@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form>
                <div class="form-group">
                  <label>Name</label>
                  <input disabled type="text" class="form-control" id="name" value="{{$task->name}}">
                </div>
                <div class="form-group">
                  <label>Content</label>
                  <input disabled type="text" class="form-control" id="content" value="{{$task->content}}">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
