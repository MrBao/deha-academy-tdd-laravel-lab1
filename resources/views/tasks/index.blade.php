@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form action="{{route('tasks.index')}}" method="GET">
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="keyword">
                        </div>
                    </div>
                    <div class="col-md-2 text-center">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </div>
            </form>
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Content</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($tasks as $task)
                    <tr>
                        <th scope="row">{{$task->id}}</th>
                        <td>
                            <a href="{{route('tasks.show', $task->id)}}" class="text-decoration-none">{{$task->name}}</a>
                        </td>
                        <td>{{$task->content}}</td>
                        <td>
                            <a href="{{route('tasks.edit', $task->id)}}" class="btn btn-warning">Edit</a>
                            <form action="{{route('tasks.destroy', $task->id)}}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
        </div>
    </div>
</div>

@endsection
