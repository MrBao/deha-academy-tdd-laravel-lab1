@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form action="{{route('tasks.update', $task->id)}}" method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" class="form-control" id="name" name="name" value="{{$task->name}}">
                </div>
                <div class="form-group">
                  <label for="content">Content</label>
                  <input type="text" class="form-control" id="content" name="content" value="{{$task->content}}">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>

@endsection
