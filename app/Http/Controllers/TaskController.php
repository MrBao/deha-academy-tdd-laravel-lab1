<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTaskRequset;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    protected $tasks;

    public function __construct(Task $tasks)
    {
        $this->tasks = $tasks;
    }

    public function index(Request $request) {
        $keyword = '';
        $tasks = $this->tasks->all();
        if(isset($request['keyword']) && !empty($request['keyword'])) {
            $keyword = $request['keyword'];
            $tasks = $this->tasks->where('name', 'like', '%'.$keyword.'%')->orWhere('content', 'like', '%'.$keyword.'%')->get();
        }
        return view('tasks.index', compact('tasks', 'keyword'));
    }

    public function show($id) {
        $task = $this->tasks->findOrFail($id);
        return view('tasks.show', compact('task'));
    }

    public function store(StoreTaskRequset $request) {
        $this->tasks->create($request->all());
        return redirect(route('tasks.index'));
    }

    public function create() {
        return view('tasks.create');
    }

    public function update(UpdateTaskRequest $request, $id) {
        $this->tasks->findOrFail($id)->update($request->all());
        return redirect(route('tasks.index'));
    }

    public function edit($id) {
        $task = $this->tasks->findOrFail($id);
        return view('tasks.edit', compact('task'));
    }

    public function destroy($id) {
        $this->tasks->findOrFail($id)->delete();
        return redirect(route('tasks.index'));
    }
}
