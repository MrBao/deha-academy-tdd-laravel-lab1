<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Http\Response;
use Tests\TestCase;

class SearchTaskTest extends TestCase
{
    /** @test */
    public function user_can_search_name_task()
    {
        $task = Task::factory()->create();
        $response = $this->get('/tasks?keyword='.$task->name);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertSee($task->name);
    }

    /** @test */
    public function user_can_search_content_task()
    {
        $task = Task::factory()->create();
        $response = $this->get('/tasks?keyword='.$task->content);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertSee($task->body);
    }
}
