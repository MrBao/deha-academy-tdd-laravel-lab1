<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_delete_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create()->toArray();
        $response = $this->delete($this->getDeleteTaskRoute($task['id']));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getListTaskRoute());
        $this->assertDatabaseMissing('tasks', $task);
    }

    /** @test */
    public function authenticated_user_can_not_delete_task()
    {
        $task = Task::factory()->create()->toArray();
        $response = $this->delete($this->getDeleteTaskRoute($task['id']));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
        $this->assertDatabaseHas('tasks', $task);
    }

    /** @test */
    public function authenticated_user_can_not_delete_task_if_task_not_exsist()
    {
        $this->actingAs(User::factory()->create());
        $taskId = -1;
        $response = $this->delete($this->getDeleteTaskRoute($taskId));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function getDeleteTaskRoute($id) {
        return route('tasks.destroy', $id);
    }

    public function getListTaskRoute() {
        return route('tasks.index');
    }
}
