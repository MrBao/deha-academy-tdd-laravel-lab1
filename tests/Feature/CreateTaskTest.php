<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateTaskTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_create_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make()->toArray();
        $response = $this->post($this->getCreateTaskRoute(), $task);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/tasks');
        $this->assertDatabaseHas('tasks', $task);
    }

    /** @test */
    public function authenticated_user_can_not_create_task()
    {
        $task = Task::factory()->make()->toArray();
        $response = $this->post($this->getCreateTaskRoute(), $task);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_not_create_task_if_field_name_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['name' => null])->toArray();
        $response = $this->post($this->getCreateTaskRoute(), $task);

        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function authenticated_user_can_see_create_task_view()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getCreateTaskViewRoute());

        $response->assertViewIs('tasks.create');
    }

    /** @test */
    public function authenticated_user_can_see_errors_in_create_task_view_if_field_name_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['name' => null])->toArray();
        $response = $this->from($this->getCreateTaskViewRoute())->post($this->getCreateTaskRoute(), $task);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getCreateTaskViewRoute());
    }

    public function getCreateTaskRoute() {
        return route('tasks.store');
    }

    public function getCreateTaskViewRoute() {
        return route('tasks.create');
    }
}
