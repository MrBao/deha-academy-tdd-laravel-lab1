<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetTaskTest extends TestCase
{
    /** @test */
    public function user_can_get_task()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getShowTaskRoute($task->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.show');
    }

    public function getShowTaskRoute($id) {
        return route('tasks.show', $id);
    }

    /** @test */
    public function user_not_can_get_task_if_task_not_exist()
    {
        $taskId = -1;
        $response = $this->get($this->getShowTaskRoute($taskId));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
