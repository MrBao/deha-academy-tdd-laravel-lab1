<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_update_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $dataUpdate = [
            'name' => $this->faker->name,
            'content' => $this->faker->text
        ];
        $response = $this->put($this->getUpdateTaskRoute($task->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('tasks', $dataUpdate);
        $response->assertRedirect(route('tasks.index'));
    }

    /** @test */
    public function authenticated_user_can_not_update_task()
    {
        $task = Task::factory()->create();
        $dataUpdate = [
            'name' => $this->faker->name,
            'content' => $this->faker->text
        ];
        $response = $this->put($this->getUpdateTaskRoute($task->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_not_update_task_if_field_name_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $dataUpdate = [
            'name' => null,
            'content' => $this->faker->text
        ];
        $response = $this->put($this->getUpdateTaskRoute($task->id), $dataUpdate);

        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function authenticated_user_can_see_update_task_view()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->get(route('tasks.edit', $task->id));

        $response->assertViewIs('tasks.edit');
    }

    public function getUpdateTaskRoute($id) {
        return route('tasks.update', $id);
    }
}
